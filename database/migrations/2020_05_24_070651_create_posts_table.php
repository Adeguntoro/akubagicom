<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            
            //Artikel
            $table->string('judul')->nullable();
            $table->longText('konten')->nullable();
            $table->string('tipe_konten')->default('artikel');

            //Quote
            $table->longText('quote')->nullable();
            $table->string('person')->nullable();
            $table->string('source')->nullable();
            
            $table->string('slug')->nullable();
            $table->longtext('tldr')->nullable();
            $table->string('kategori')->default('uncategorize');
            $table->string('artikel_tag')->nullable();
            $table->string('image_feature')->nullable();
            $table->string('keyword')->nullable();
            $table->string('artikel_status')->nullable();
            $table->string('page_counter')->default('0');
            $table->string('page_identifier')->nullable();
            
            $table->softDeletes(); 

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
