<?php

use Illuminate\Database\Seeder;

use App\User;
use Illuminate\Support\Facades\Hash;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'name' => 'Ade Guntoro',
            'email' => 'guntoroade@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('1234'),
        ]); //->assignRole('Master');
    }
}
