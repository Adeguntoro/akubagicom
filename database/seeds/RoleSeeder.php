<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        # permissions
        Permission::create(['name' => 'Update Password']);
        Permission::create(['name' => 'Update Profile']);

        Permission::create(['name' => 'Delete Staff']);
        Permission::create(['name' => 'Delete Admin']);
        
        Permission::create(['name' => 'Create Staff']);
        Permission::create(['name' => 'Create Admin']);
        
        Permission::create(['name' => 'Create Post']);
        Permission::create(['name' => 'Update Post']);
        Permission::create(['name' => 'Delete Post']);
        
        Permission::create(['name' => 'Add Item']);
        Permission::create(['name' => 'Update Item']);
        Permission::create(['name' => 'Delete Item']);

        Permission::create(['name' => 'Add Categori']);
        Permission::create(['name' => 'Update Categori']);
        Permission::create(['name' => 'Delete Categori']);

        # roles
        
        Role::create(['name' => 'Master'])->givePermissionTo(['Create Admin', 'Delete Admin', 'Update Profile', 'Update Password']);
        Role::create(['name' => 'Admin'])->givePermissionTo([
            'Create Staff',
            'Delete Staff',
            'Update Profile',
            'Update Password',
            'Create Post',
            'Update Post',
            'Delete Post',
            'Add Categori',
            'Update Categori',
            'Delete Categori'
        ]);
        Role::create(['name' => 'Staff'])->givePermissionTo([
            'Update Profile',
            'Update Password',
            'Add Item',
            'Update Item',
            'Delete Item'
        ]);

        # users
        User::create([
            'name' => 'Master',
            'email' => 'master@mail.com',
            'password'  => Hash::make('1234'),
            //'nomor_iden' => bin2hex(random_bytes(5)),
            //'status' => '1'
        ])->assignRole('Master');

        User::create([
            'name' => 'Admin',
            'email' => 'admin@mail.com',
            'password'  => Hash::make('1234'),
            //'nomor_iden' => bin2hex(random_bytes(5)),
            //'status' => '1'
        ])->assignRole('Admin');

        User::create([
            'name' => 'Staff',
            'email' => 'staff@mail.com',
            'password'  => Hash::make('1234'),
            //'nomor_iden' => bin2hex(random_bytes(5)),
            //'status' => '1'
        ])->assignRole('Staff');
    }
}
