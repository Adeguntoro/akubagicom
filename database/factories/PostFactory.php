<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Post;
use Illuminate\Support\Str;

$factory->define(Post::class, function (Faker $faker) {
    return [
        //
        //'title' => $faker->name,
        //'content' => '<p>'.$faker->text($maxNbChars = 400).'</p><p>'.$faker->text($maxNbChars = 850).'</p><p>'.$faker->text($maxNbChars = 200).'</p>',
        'judul' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'konten' => '<p>'.$faker->text($maxNbChars = 400).'</p><p>'.$faker->text($maxNbChars = 850).'</p><p>'.$faker->text($maxNbChars = 200).'</p>',
        'user_id' => '4',
        'slug' => Str::slug($faker->name).'-'.Str::random(6),
        'artikel_status' => 'publish',
        'tldr' => $faker->text($maxNbChars = 150),
        'page_counter' => '0',
        'page_identifier' => bin2hex(random_bytes(32)),
        'tipe_konten' => $faker->randomElement($array = array ('artikel','quote')),
        'kategori' => $faker->randomElement($array = array ('artikel','tutorial', 'quote', 'review', 'preview', 'laravel'))
    ];
});
