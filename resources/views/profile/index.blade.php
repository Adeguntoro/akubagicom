@extends('layouts.app')

@section('css')
<style>
    .user-avatar {
        width: 240px;
        height: 240px;
        margin: 2em auto;
        display: block;
        border-radius: 50%;
        -webkit-user-drag: none;
        -moz-user-drag: none;
        -ms-user-drag: none;
        -o-user-drag: none;
        user-drag: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        -o-user-select: none;
        user-select: none;
    }
</style>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            <div class="card">

                <div class="card-body">
                    <img src="{{ $user->avatar }}" class="card-img-top user-avatar" alt="...">
                    <form action="/update/profile" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">Name</label>

                            <div class="col-md-10">
                                <input id="name" type="text" class="form-control" name="name" required value="{{ $user->name }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-2 col-form-label text-md-right">Email</label>

                            <div class="col-md-10">
                                <input id="email" type="text" class="form-control" name="email" required value="{{ $user->email }}">
                            </div>
                        </div>
                        {{--
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">Avatar</label>
                            <div class="col-md-10 d-inline-block">
                                <input type="file" name="avatar" aria-describedby="avatar" accept="image/jpeg">
                                <small id="avatar" class="form-text text-muted">Max 2MB, format JPG.</small>
                            </div>
                        </div>
                        --}}
                        <div class="text-center">
                            <button class="btn btn-primary">Update Profile</button>
                        </div>
                        
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>
@endsection
