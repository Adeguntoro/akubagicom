<!doctype html>
<html lang="en" class="h-100">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Akubagi</title>
        
        
    
        <link rel="icon" href="{{ url('HatchfulExport-All/favicon.png') }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
        <link rel="stylesheet" href="{{ asset('fonts/font.css') }}">
        
        
        <style>
            body{
                background-color: #F8FAFC;
            }
            
        </style>
    </head>
    <body>
        @include('artikel.navbar')
        <div class="container pt-4 pb-4 mb">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <h1 class="heading">Semua Artikel</h1>
                    @foreach($posts as $post)
                    <div class="card mb-3 shadow-sm border-0">
                        <div class="card-body">
                            <div class="mb-3 d-flex justify-content-between">
                                <div class="pr-3">
                                    <h2 class="card-title title">
                                    <a class="url" href="/read/{{ $post->kategori }}/{{ $post->slug }}">{{ $post->judul }}</a>
                                    </h2>
                                    <p class="card-text">{{ $post->tldr }}</p>
                                    <div class="card-text">
                                        Diterbitkan {{ $post->created_at->diffForHumans() }} di <a href="/kategori/{{ $post->kategori }}">{{ $post->kategori }}</a>.
                                    </div>
                                </div>
                                @if( empty($post->image_feature) )
                                <img height="120" src="{{ url('/HatchfulExport-All/linkedin_banner_image_2.png') }}">
                                @else
                                <img height="120" src="{{ $post->image_feature }}">
                                @endif
                            </div>
                        </div>
                    </div>
                    
                    @endforeach
                    {{ $posts->links() }}
                </div>
            </div>
        </div>
        
        <script src="{{ asset('js/app.js') }}"></script>
        <script>
            
            jQuery( document ).ready(function() {
                $(window).scroll(function(){
                    $('.topnav').toggleClass('unsrink', $(this).scrollTop() > 90);
                });
            });
            
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
        
    </body>
</html>