<!doctype html>
<html lang="en" class="h-100">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Akubagi</title>
        


        {{--
        <link rel="icon" href="{{ url('HatchfulExport-All/favicon.png') }}">
        <link rel="stylesheet" href="{{ asset('bs4/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('fonts/font.css') }}">
        --}}
        <link rel="stylesheet" href="{{ asset('css/app.css')}}">
        
        <style>
            body{
                font-family: "Gudea";
                padding-top: 80px;
                background-color: #f8fafc;
            }
            
                    
            .unsrink{
                min-height: 50px !important;
                transition: 0.5s;

            }
            .navbar{
                min-height: 80px;
                transition: 0.5s;
            }
            p.card-text{
                font-size: 20px !important;    
            }
            .card-title{
                font-family: 'Gudea Bold';
            }
            
            .heading{
                /* font-family: 'Gudea Bold'; */
                color: #0A91AB !important;
            }
            
            a {
                color: #0A91AB !important;
                text-decoration: none;
                background-color: transparent;
                -webkit-text-decoration-skip: objects;
            }
            
            a:not([href]):not([tabindex]) {
                color: inherit;
                text-decoration: none;
            }
            a:not([href]):not([tabindex]):hover, a:not([href]):not([tabindex]):focus {
                color: inherit;
                text-decoration: none; 
            }
            
            a:not([href]):not([tabindex]):focus {
                outline: 0;
            }

            .bg-primary { 
                background: #77A9BC !important;
            }
            
            blockquote {
                font-family: 'PlayfairDisplay-Medium';
                font-size: 30px !important;
                color: white;
            }
            .blockquote-footer {
                display: block;
                font-size: 16px !important;
                color: white;
            }
            p{
                font-size: 20px;
            }
            .btn-outline-success {
                color: #0A91AB;
                border-color: #0A91AB;
            }
            .btn-outline-success:hover {
                color: #fff;
                background-color: #0A91AB;
                border-color: #0A91AB;
            }
        </style>
    </head>
    <body>
        @include('artikel.navbar')
        
        @yield('content')
        
        <script src="{{ asset('js/app.js')}}"></script>
        <script>
            
            jQuery( document ).ready(function() {
                $(window).scroll(function(){
                    $('.topnav').toggleClass('unsrink', $(this).scrollTop() > 90);
                });
            });
            
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
        
    </body>
</html>