<!doctype html>
<html lang="en" class="h-100">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Akubagi - {{ $post->judul }}</title>
        
        @include('meta::manager')
    
        <link rel="icon" href="{{ url('HatchfulExport-All/favicon.png') }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
        <link rel="stylesheet" href="{{ asset('fonts/font.css') }}">
        <link rel="stylesheet" href="{{asset('vendor/laraberg/css/laraberg.css')}}">
        
        
        <style>
            body {
                background-color: #F8FAFC;
                font-family: 'Lato Regular';
            }
            
        </style>
    </head>
    <body>
        @include('artikel.navbar')
        <div class="container pt-4 pb-4 mb-2">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    
                    <div class="d-flex align-items-center">
                        <img class="rounded-circle" src="http://localhost:8000/HatchfulExport-All/sequare_44.png" width="50">
                        <h5 class="ml-3 author">{{$post->user->name}} <span class="d-block"><small>Diterbitkan {{ $post->created_at->diffForHumans() }} di <a href="/kategori/{{ $post->kategori }}">{{ $post->kategori }}</a>&middot;</small></span>
                        </h5>
                    </div>
                    
                    <h1 class="display-4 secondfont pt-4">{{ $post->judul }}</h1>
                    <p class="tldr">
                    @if ( $post->tldr != 'null')
                        {{ $post->tldr}} 
                    @endif
                    </p>
                </div>

            </div>

            <div class="row justify-content-center pb-4">
                <div class="col-md-8">
                    <article class="article-post">
                        {!! $post->konten !!}
                    </article>
                    <hr>
                    @auth
                    <i class="fas fa-pen"></i>  <a href="{{ route('post.edit', $post->id)}}">Edit</a>
                    @endauth
                </div>

            </div>
            

            <div class="row justify-content-center pt-4 pb-4">
                <div class="col-md-8">
                    <div id="disqus_thread"></div>
                </div>
            </div>

        </div>
        
        <script src="{{ asset('js/app.js') }}"></script>
        <script>
            
            jQuery( document ).ready(function() {
                $(window).scroll(function(){
                    $('.topnav').toggleClass('unsrink', $(this).scrollTop() > 90);
                });
            });
            
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
        
    </body>
</html>