@extends('dashboard.app')

@section('css')
<link rel="stylesheet" href="{{asset('vendor/laraberg/css/laraberg.css')}}">
<style>
    div.components-menu-group{
        display: block !important;
    }
</style>
@endsection
@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="card shadow h-100 py-2">
        <div class="card-body">
            <form enctype="multipart/form-data" method="POST" action="{{ route('post.update', $post->id) }}">
                @csrf
                @method('PUT')
                
                <div class="form-group">
                    <div class="input-group input-group-md">
                        <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" name="judul" placeholder="Judul" value="{{ $post->judul ?? old('judul') }}">
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Status saat ini : {{ $post->artikel_status }} </label>
                    <select class="form-control" id="exampleFormControlSelect1" name="status" required>
                        <option value="publish">Terbitkan Sekarang</option>
                        <option value="draft">Draft / Simpan</option>
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Kategori</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="kategori" required>
                        @foreach($kategori as $data)
                        <option value="{{ $data->slug }}">{{ $data->judul }}</option>
                        @endforeach
                    </select>
                </div>
                
                <div class="form-group">
                    <div class="input-group input-group-md">
                        <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" name="keyword" placeholder="Keyword" value="{{ $post->keyword ?? old('keyword') }}">
                    </div>
                </div>
                
                <div class="form-group">
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" name="ringkasan" placeholder="Ringkasan">{{ $post->tldr ?? old('ringkasan') }}</textarea>
                </div>
                
                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="customFile" accept="image/jpeg" name="feature">
                        <input type="text" class="form-control" name="current_feature" hidden value="{{ $data->image_feature }}">
                        <label class="custom-file-label" for="customFile">{{ $data->image_feature }}</label>
                    </div>
                </div>
                
                
                
                <div class="form-group">
                    <textarea id="konten" name="konten" hidden>{!! $post->konten ?? old('konten') !!}</textarea>
                </div>
                
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{ asset('vendor/bs-custom-file-input/dist/bs-custom-file-input.min.js') }}"></script>
<script src="{{ asset('vendor/react/umd/react.production.min.js') }}"></script>
<script src="{{ asset('vendor/react-dom/umd/react-dom.production.min.js') }}"></script>
<script src="{{ asset('vendor/laraberg/js/laraberg.js') }}"></script>
<script>
    bsCustomFileInput.init();
    const options = {
        laravelFilemanager: {
            prefix: '/upload_file_untuk_akubagi'
        }
    }
    Laraberg.init('konten', options)
</script>
@endsection
