@extends('dashboard.app')

@section('css')
<link rel="stylesheet" href="{{ asset('vendor/DataTables/dataTables.bootstrap4.css') }}">
<style>
    .page-item.active .page-link {
        color: #fff;
        background-color: #0A91AB;
        border-color: #0A91AB;
    }
</style>
@endsection
@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif
    
    <div class="card shadow h-100 py-2">
        

        
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nomor</th>
                            <th width="36%">Judul</th>
                            <th>Viewer</th>
                            <th width="10%">Kategori</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('vendor/DataTables/dataTables.bootstrap4.min.js') }}"></script>

<script>

$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $('#dataTable').DataTable({
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('post.index') }}",
            type: 'GET',
        },
        columns: [
            {data: 'id',name: 'id', 'visible': false},
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
            {data: 'judul', name: 'judul' },
            {data: 'page_counter', name: 'page_counter' },
            {data: 'kategori', name: 'kategori' },
            {data: 'action', name: 'action', orderable: false}
        ],
        order: [[0, 'desc']]
    });
    
    $('body').on('click', '#delete-user', function () {
        var user_id = $(this).data("id");
        if(confirm("Are You sure want to delete !")){
            $.ajax({
                type: "DELETE",
                url: "{{ url('dashboard/hapus') }}/"+user_id,
                success: function (data) {
                    var oTable = $('#dataTable').dataTable(); 
                    oTable.fnDraw(false);
                    console.log('Sukses !');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    });
    
});

</script>
@endsection
