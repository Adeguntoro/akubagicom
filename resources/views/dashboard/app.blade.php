<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Akubagi - Dashboard </title>

        <!-- Custom fonts for this template-->

        <!-- Custom styles for this template-->
        <link href="{{ asset('vendor/fontawesome-free-5.10.1-web/css/all.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/sb-admin-2.css') }}" rel="stylesheet">
        <link href="{{ asset('fonts/font.css') }}" rel="stylesheet">
        <style>
            .bg-gradient-primary {
                background-color: #0A91AB;
                background-image: '' !important;
            }
            .sidebar-dark #sidebarToggle {
                background-color: white;
            }
            .sidebar-dark #sidebarToggle::after {
                color: #0A91AB;
            }
            .sidebar-dark #sidebarToggle:hover {
                background-color: #F0F0F0;
            }
            .sidebar-dark.toggled #sidebarToggle::after {
                color: #0A91AB;
            }
            .btn-primary {
                color: #fff;
                background-color: #0A91AB;
                border-color: #0A91AB;
            }
            .btn-primary:hover {
                color: #0A91AB;
                background-color: white;
                border-color: #0A91AB;
            }
            .sidebar-dark .sidebar-heading {
                color: white;
            }
            .sidebar-dark .nav-item .nav-link i {
                color: white;
            }
            
            body {
                color: #3a3b45 !important;
                font-family: 'Lato Regular';
            }
            
            .sidebar .nav-item .collapse .collapse-inner .collapse-item.active, .sidebar .nav-item .collapsing .collapse-inner .collapse-item.active {
                color: #0A91AB;
                font-weight: 700;
            }
            .sidebar .sidebar-heading {
                font-weight: 100;
            }
            .sidebar .nav-item.active .nav-link {
                font-weight: 100;
            }
            
            .sidebar .nav-item .collapse .collapse-inner .collapse-item.active, .sidebar .nav-item .collapsing .collapse-inner .collapse-item.active {
                color: #0A91AB;
                font-weight: 100 !important; 
            }
            img.image-icon{
                height: 40px;
            }
            .sidebar-brand-text{
                font-family: 'RobotoSlab Medium' !important;
            }
            
            
        </style>
        @yield('css')
    </head>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            @include('dashboard.sidebar')
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    @include('dashboard.navbar')
                    <!-- End of Topbar -->

                    <!-- Begin Page Content -->
                    {{-- @include('dashboard.index') --}}
                    
                    @yield('content')
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Your Website 2019</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="{{ asset('js/app.js') }}"></script>
        
        <!-- Core plugin JavaScript-->
        <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

        <!-- Custom scripts for all pages-->
        <script src="{{ asset('vendor/sb-admin-2.min.js') }}"></script>
        @yield('js')
    </body>
</html>