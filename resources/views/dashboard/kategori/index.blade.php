@extends('dashboard.app')

@section('css')
<link rel="stylesheet" href="{{ asset('vendor/DataTables/dataTables.bootstrap4.css') }}">

@endsection
@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif
    <div class="card shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="25%">Judul</th>
                            <th>Viewer</th>
                            <th width="50%">Kategori</th>
                            <th width="25%">Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($result as $data)
                        <tr>
                            <td>{{ $data->judul }}</td>
                            <td>{{ $data->slug }}</td>
                            <td>{{ $data->keterangan }}</td>
                            <td>Deleted</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('vendor/DataTables/dataTables.bootstrap4.min.js') }}"></script>

<script>

$(document).ready(function() {
    
    $('#dataTable').DataTable({
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
    });
    
});

</script>
@endsection