@extends('dashboard.app')

@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="card shadow h-100 py-2">
        <div class="card-body">
            <form method="post" action="{{ route('categori.store')}}">
                @csrf
                
                <div class="form-group">
                    <div class="input-group input-group-md">
                        <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" name="name" placeholder="Judul" required>
                    </div>
                </div>
                
                {{--
                <div class="form-group">
                    <div class="input-group input-group-md">
                        <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" name="slug" placeholder="URL bersifat opsional">
                    </div>
                </div>
                --}}

                
                <div class="form-group">
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" name="keterangan" placeholder="Ringkasan" required>{{ old('ringkasan') }}</textarea>
                </div>
                
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
