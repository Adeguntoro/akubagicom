<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false,
    'verify' => false,
    'reset' => false
]);

Route::get('/', 'ArtikelController@index');
Route::get('/read/{kategori}/{slug}', 'ArtikelController@show'); // This error
Route::get('/kategori/{name}', 'ArtikelController@kategori'); // This error

// This also fine if i put after prefix.
// Route::get('/', 'ArtikelController@index');
// Route::get('/{kategori}/{slug}', 'ArtikelController@show');

Route::resource('post', 'PostController');
Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'verified'])->prefix('dashboard')->group(function () {
    Route::get('/', 'Dashboard@index');
    Route::resource('post', 'PostController');
    Route::resource('categori', 'KategoriController');
    Route::delete('hapus/{id}', 'PostController@destroy');
    
    Route::view('role', 'role');
    Route::get('role', function(){
        return view('role');
    });
});
Route::prefix('demo')->group(function () {
    // Route::view('berg', 'laraberg');
});

/*
Route::get('/notif', function() {
    $user = \App\User::first();
    $user->notify(new \App\Notifications\Daftar);
});
*/