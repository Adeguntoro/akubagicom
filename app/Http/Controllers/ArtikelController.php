<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;

use App\Events\PageCounter;

class ArtikelController extends Controller
{
    //
    public function index()
    {
        //Webmaster
        /*
        $datas = Webs::all();
        foreach($datas as $data){
            config(['seotools.meta.webmaster_tags.'.$data->name.'' => $data->value]);
        }
        */

        $posts = Post::latest()
            ->where('artikel_status', 'publish')
            ->where('tipe_konten', '<>', 'quote')
            ->where('kategori', '<>', 'quote')
            ->simplepaginate(10);
        return view('artikel.index', compact('posts'));
        
    }

    public function show($kategori, $slug)
    {
        //
        $post = Post::where('slug', $slug)->where('kategori', $kategori)->firstorfail();
        config(['meta.title' => $post->judul ]);
        config(['meta.description' => $post->tldr ]);
        config(['meta.image' => $post->image_feature]);
        config(['meta.author' => $post->user->name]);
        config(['meta.twitter_site' => '@ade_fatality']);
        config(['meta.keywords' => $post->keyword]);
        config(['meta.fb_app_id' => 'adasda']);
        
        event(new PageCounter($post)); //trigger event

        return view('artikel.artikel', compact('post'));
    }

    public function kategori($name = 'artikel')
    {
        //
        
        $data = Post::latest()->where('kategori', $name)->simplepaginate(5);
        
        if( count($data) > 0 ){
            return view('artikel.kategori', compact([
                'name',
                'data'
            ]));
        } else {
            return view('artikel.no_kategori');
        }
    }

}
