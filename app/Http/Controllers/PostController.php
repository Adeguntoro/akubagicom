<?php

namespace App\Http\Controllers;

use App\Post;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Auth;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Kategori;

use GDText\Box;
use GDText\Color;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request->ajax()) {
            $data = Post::latest()->get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<a href="/dashboard/post/'.$row->id.'/edit" data-original-title="Edit" class="edit btn btn-primary">Edit</a>';
                $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger deleteProduct" id="delete-user">Delete</a>';
                $btn = $btn.' <a href="/read/'.$row->kategori.'/'.$row->slug.'" class="btn btn-warning" target="_blank">View</a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        };
        return view('dashboard.post.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //$kategori = Kategori::all();
        $kategori = Kategori::orderBy('judul', 'asc')->get();
        return view('dashboard.post.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return response()->json($request->all());
        $user   = Auth::user();
        
        $image  = $request->file('feature');
        $judul  = $request->judul;

        if($request->hasfile('feature')){
            $namefile   = '/feature/'.bin2hex(random_bytes(32)).'_'.$image->getClientOriginalName();
            $simpan     = $image->move(public_path('feature'), $namefile);
        } else {
            $im         = @imagecreatefromjpeg(public_path('HatchfulExport-All/blank.jpg'));
            $text       = $request->judul;
            $textbox    = new Box($im);
            $textbox->setFontSize(80);
            
            $textbox->setFontFace(public_path('fonts/Gudea/Gudea-Bold.ttf'));
        
            $textbox->setFontColor(new Color(10, 145, 171)); // black
            $textbox->setBox(
                0,  // distance from left edge
                0,  // distance from top edge
                imagesx($im), // textbox width, equal to image width
                imagesy($im)  // textbox height, equal to image height
            );
    
            $textbox->setTextAlign('center', 'center');
            // it accepts multiline text
            $textbox->draw($text);

            $namefile = '/feature/'.Str::slug($request->judul, '-').'.jpg';
            $name = public_path($namefile); //this saves the image inside uploaded_files folder
            imagepng($im, $name, 9);
        }

        $post                   = New Post;
        $post->user_id          = $user->id;
        $post->judul            = $request->judul;
        $post->konten           = $request->konten;
        $post->kategori         = $request->kategori;
        $post->keyword          = $request->keyword;
        $post->slug             = Str::slug($request->judul, '-').'-'.Str::random(6);
        $post->artikel_status   = $request->status;
        $post->tldr             = $request->ringkasan;
        $post->page_identifier  = bin2hex(random_bytes(15));
        $post->image_feature    = $namefile;
        $post->save();        
        
        return redirect('dashboard/post')->with('status', 'Artikel berhasil ditambahkan!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    //public function edit(Post $post)
    public function edit($id)
    {
        //
        $kategori = Kategori::orderBy('judul', 'asc')->get();
        $post = Post::find($id);
        //return response()->json($data);
        return view('dashboard.post.edit', compact(['post', 'kategori']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, Post $post)
    public function update(Request $request, $id)
    {
        //

        $user                   = Auth::user();

        $image  = $request->file('feature');
        $judul  = $request->judul;

        $namefile = $request->current_feature;
        
        if($request->hasfile('feature')){
            $namefile   = '/feature/'.bin2hex(random_bytes(32)).'_'.$image->getClientOriginalName();
            $simpan     = $image->move(public_path('feature'), $namefile);
        } else {
            $im         = @imagecreatefromjpeg(public_path('HatchfulExport-All/blank.jpg'));
            $text       = $request->judul;
            $textbox    = new Box($im);
            $textbox->setFontSize(80);
            
            $textbox->setFontFace(public_path('fonts/Roboto_Slab/static/RobotoSlab-Regular.ttf'));
        
            $textbox->setFontColor(new Color(10, 145, 171)); // black
            $textbox->setBox(
                0,  // distance from left edge
                0,  // distance from top edge
                imagesx($im), // textbox width, equal to image width
                imagesy($im)  // textbox height, equal to image height
            );
    
            $textbox->setTextAlign('center', 'center');
            // it accepts multiline text
            $textbox->draw($text);

            $namefile = '/feature/'.Str::slug($request->judul, '-').'.jpg';
            $name = public_path($namefile); //this saves the image inside uploaded_files folder
            imagepng($im, $name, 9);
        }
        
        $post                   = Post::find($id);
        $post->judul            = $request->judul;
        $post->konten           = $request->konten;
        $post->kategori         = $request->kategori;
        $post->artikel_status   = $request->status;
        $post->tldr             = $request->ringkasan;
        $post->keyword          = $request->keyword;
        $post->image_feature    = $namefile;

        $post->update();        
        
        return redirect('dashboard/post')->with('status', 'Artikel berhasil diupdate');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    //public function destroy(Post $post)
    public function destroy($id)
    {
        //
        Post::findOrFail($id)->delete();
    }
}
