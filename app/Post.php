<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use VanOns\Laraberg\Models\Gutenbergable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    //
    use Gutenbergable;
    use SoftDeletes;

    //protected $table = 'posts';
    protected $fillable = [
        'judul',
        'konten',
        'slug',
        'artikel_tag',
        'artikel_status',
        'image_feature',
        'kategori',
        'tldr',
        'author',
        'page_counter',
        'page_identifier',
        'tipe_konten',
        'keyword'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    protected $dates = [
        'deleted_at',
        'updated_at'
    ];
}
