<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Post;
use App\Events\PageCounter;
use Illuminate\Http\Request;

class ViewCounter
{

    public function __construct()
    {
        //
    }

    public function handle(PageCounter $event)
    {
        //
        if( ! session()->has($event->post->page_identifier)) {
            $count = $event->post;
            $count->increment('page_counter');
            $count->page_counter + 1;
            $this->storePost($event);
        }
    }
    private function storePost($event) {
        session()->put($event->post->page_identifier, $event->post->page_identifier);
        //app('log')->info(array('Page Identifier' => $event->post->page_identifier, 'Post Slug URL' => $event->post->slug));
    }

}
